import * as redisStore from 'cache-manager-redis-store';

import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProbabilisticCacheController } from './probabilistic-cache/probabilistic-cache.controller';
import { ProbabilisticCacheService } from './probabilistic-cache/probabilistic-cache.service';
import { ProbabilisticCacheEntity } from './probabilistic-cache/probabilistic-cache.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST,
      port: parseInt(process.env.REDIS_PORT),
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
    
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    
      entities: ['dist/**/*.entity.js'],
    
      migrationsTableName: 'migration',
    
      migrations: ['src/migration/*.ts'],
    
      cli: {
        migrationsDir: 'src/migration',
      },

      synchronize: true
    }),
    TypeOrmModule.forFeature([ProbabilisticCacheEntity])
  ],
  controllers: [ProbabilisticCacheController],
  providers: [ProbabilisticCacheService]
})
export class AppModule {}
