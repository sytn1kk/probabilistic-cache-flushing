import { ProbabilisticCacheEntity } from './probabilistic-cache.entity';

export class ProbabilisticCacheDto {
    public value: ProbabilisticCacheEntity;
    public delta: number;
    public expired: number;

    constructor(value: ProbabilisticCacheEntity, delta, expired) {
        this.delta = delta;
        this.expired = expired;
        this.value = value;
    }
}