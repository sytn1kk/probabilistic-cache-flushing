import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'probabilistic_cache' })
export class ProbabilisticCacheEntity {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ type: 'jsonb' })
    public json: any;

    constructor(json: any) {
        this.json = json;
    }
}