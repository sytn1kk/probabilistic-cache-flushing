# Probabilistic Cache Flushing
## Node.js, PostgreSQL, Redis, Artillery

## Run

- ```git clone https://gitlab.com/sytn1kk/probabilistic-cache-flushing.git```
- ```cd probabilistic-cache-flushing```
- ```docker run --network=probabilistic-cache-flushing_default --rm -it \
      -v "$(pwd)/artillery/":/scripts \
      artilleryio/artillery:latest \
      run /scripts/load-test.yml```

## Load testing results

```
--------------------------------------
Metrics for period to: 21:45:20(+0000) (width: 9.953s)
--------------------------------------

vusers.created_by_name.0: ................................... 787
vusers.created.total: ....................................... 787
vusers.failed: .............................................. 140
vusers.completed: ........................................... 98
vusers.session_length:
  min: ...................................................... 7358.1
  max: ...................................................... 13028.9
  median: ................................................... 9047.6
  p95: ...................................................... 11050.8
  p99: ...................................................... 12459.8
http.request_rate: .......................................... 94/sec
http.requests: .............................................. 900
http.codes.201: ............................................. 107
http.responses: ............................................. 205
http.codes.200: ............................................. 98
http.response_time:
  min: ...................................................... 23
  max: ...................................................... 10266
  median: ................................................... 7260.8
  p95: ...................................................... 9607.1
  p99: ...................................................... 9607.1
errors.ECONNRESET: .......................................... 3
errors.ETIMEDOUT: ........................................... 137
```

## Screenshots
![Load test](https://i2.paste.pics/cec228d9886ba084c717ecebfd117425.png "Load test")
![Cache logic](https://i2.paste.pics/82312947b59d8d04d66c79166f9d7ba2.png "Cache logic")

